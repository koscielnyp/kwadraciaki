class Man {

	constructor() {
		this.posX = 0;
		this.posY = 0;
		this.health = 100;
		this.mobility = 1;
		this.damage = 10;
		this.face = "gfx/arnold.png";
	}
	
	spawn(x, y) {
		var playField = document.getElementById("playField");		
		var cell = playField.querySelector(".playCell[data-x = '"+ x +"'][data-y = '" + y +"']");
		var img = document.createElement("img");
		img.setAttribute("src", this.face);
		img.setAttribute("width", "20");
		img.setAttribute("height", "20");
		cell.appendChild(img);	
        this.posX = x;
        this.posY = y;
	}
	
	move() {
		
		var direction = function(){
            var x = Math.ceil(Math.random() * 10);
            while(x > 8){
                x = direction();
            }
            return x;
        }
		
        var playField = document.getElementById("playField");		
		var cell = playField.querySelector(".playCell[data-x = '"+ this.posX +"'][data-y = '" + this.posY +"']");
        cell.innerHTML="";
        
        switch(direction()){
            case 1: //do gory
                this.posY = this.posY - 1;
                break;
            case 2: //do gory prawo
                this.posY = this.posY - 1;
                this.posX = this.posX + 1;
                break;
            case 3: //do prawo
                this.posX = this.posX + 1;
                break;
            case 4: //do dolu prawo
                this.posX = this.posX + 1;
                this.posY = this.posY + 1;
                break;
            case 5: //do dolu
                this.posY = this.posY + 1;
                break;
            case 6: //do dolu lewo
                this.posX = this.posX - 1;
                this.posY = this.posY + 1;
                break;
            case 7: //do lewo
                this.posX = this.posX - 1;
                break;
            case 8: //do gory lewo
                this.posX = this.posX - 1;
                this.posY = this.posY - 1;
                break; 
        }
		
		if (this.posX > main_config.sizeX) {
			this.posX = 1;
		}
		if (this.posX < 1) {
			this.posX = main_config.sizeX;
		}
		if (this.posY > main_config.sizeY) {
			this.posY = 1;
		}
		if (this.posY < 1) {
			this.posY = main_config.sizeY;
		}
        
        this.spawn(this.posX, this.posY);
	}
	
	moving(time) {
		var self = this;
		setInterval( function(){self.move();}, time );
	}

}